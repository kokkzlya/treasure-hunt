package main

import "fmt"

// Point represents point of coordinates
type Point struct {
	X int
	Y int
}

func (p Point) Next(d Direction) Point {
	dest := p
	switch d {
	case DNorth:
		dest.Y--
	case DEast:
		dest.X++
	case DSouth:
		dest.Y++
	case DWest:
		dest.X--
	}

	return dest
}

// Map represents the map of the treasure world
type Map [][]*MapBlock

func (m Map) Get(x, y int) *MapBlock {
	return m[y][x]
}

// MapComponent represents component of the map
type MapComponent interface {
	fmt.Stringer
	isComponent() bool
}

// MapBlock is a MapComponent that has a MapComponent as its content
type MapBlock struct {
	Content MapComponent
}

func (b *MapBlock) isComponent() bool {
	return true
}

func (b *MapBlock) AsObstacle() (*Obstacle, bool) {
	o, ok := b.Content.(*Obstacle)
	return o, ok
}

func (b *MapBlock) AsGround() (*Ground, bool) {
	g, ok := b.Content.(*Ground)
	return g, ok
}

func (b MapBlock) String() string {
	return b.Content.String()
}

// Obstacle represents an obstacle '#' in the map.
type Obstacle struct {
	// BaseComponent
	Point
}

func (o *Obstacle) String() string {
	return "#"
}

func (o *Obstacle) isComponent() bool {
	return true
}

// Treasure represents a treasure. The symbol '$' will be appeared
// if the player has already discovered it.
type Treasure struct {
	// BaseComponent
	Point

	Discovered bool
}

func (t Treasure) String() string {
	if !t.Discovered {
		return "."
	} else {
		return "$"
	}
}

func (t *Treasure) isComponent() bool {
	return true
}

// Ground represents a ground '.' block of the map.
// It becomes a clear path for the character.
type Ground struct {
	// BaseComponent
	Point

	Treasure *Treasure
}

func (g *Ground) ContainsTreasure() bool {
	return g.Treasure != nil
}

func (g Ground) String() string {
	if g.ContainsTreasure() {
		return g.Treasure.String()
	}

	return "."
}

func (g *Ground) isComponent() bool {
	return true
}

// Character represens a character 'X' in the game.
type Character struct {
	Point
	HeadTo Direction
}

func (c *Character) Move(p Point) {
	c.Point = p
}

func (c *Character) isComponent() bool {
	return true
}

func (c Character) String() string {
	return "X"
}

// Direction represents the direction.
type Direction int

const (
	DNorth Direction = iota
	DEast
	DSouth
	DWest
)
