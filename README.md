# Treasure Hunt Game
## How to build

```
$ sh build.sh
```

## How to play

1. Run the app
    ```
    $ ./main
    ```
1. Use arrows to move. If your character doesn't move after you press an arrow, please press again, the character needs to change its head direction.
1. Use `X` key to find/reveal the tresure

## How to use custom map

1. Create a usual text file, for example `mymap.txt`
1. Write your own map, for example
    ```
    ########
    #......#
    #.###..#
    #...#.##
    #X#....#
    ########
    ```
    Note that `#` represents an obstacle, `.` represents clear path, and `X` represents player
1. Run `main` with `--file=<your-map-location>` flag:
    ```
    $ ./main --file=mymap.txt
    ```