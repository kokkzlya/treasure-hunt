module gitlab.com/kokkzlya/treasure-hunt

go 1.15

require (
	github.com/eiannone/keyboard v0.0.0-20200508000154-caf4b762e807
	github.com/pborman/getopt/v2 v2.1.0
	golang.org/x/sys v0.0.0-20210511113859-b0526f3d8744 // indirect
)
