package main

import (
	"bufio"
	"context"
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"sync"
	"time"

	"github.com/eiannone/keyboard"
	"github.com/pborman/getopt/v2"
)

// TreasureHunt or a "game"
type TreasureHunt struct {
	Ctx     context.Context
	Cancel  context.CancelFunc
	Blocks  Map
	Grounds []*Ground
	Char    *Character

	TreasureIsFound bool
}

func (g *TreasureHunt) MoveChar(d Direction) {
	defer func() { g.Char.HeadTo = d }()

	if g.Char.HeadTo != d {
		return
	}

	dest := g.Char.Point.Next(d)

	block := g.Blocks.Get(dest.X, dest.Y)
	_, ok := block.AsGround()
	if !ok { // if not ground then don't move
		return
	}

	g.Char.Point = dest
}

func (g *TreasureHunt) Dig() {
	dest := g.Char.Point.Next(g.Char.HeadTo)
	block := g.Blocks.Get(dest.X, dest.Y)
	gr, ok := block.AsGround()
	if !ok { // if not ground then don't move
		return
	}

	if gr.ContainsTreasure() {
		gr.Treasure.Discovered = true
		g.TreasureIsFound = true
		g.Render()
		g.Cancel()
	}
}

func (g *TreasureHunt) Render() {
	ri, ci := 0, 0
	for ri = range g.Blocks {
		for ci = range g.Blocks[ri] {
			fmt.Printf("\033[%d;%dH", ri+1, ci+1)
			fmt.Print(g.Blocks[ri][ci])
		}
	}

	fmt.Printf("\033[%d;%dH", g.Char.Y+1, g.Char.X+1)
	fmt.Print(g.Char)

	fmt.Printf("\033[%d;%dH", ri+5, 1)
	fmt.Println("Find the treasure! Press X key to dig the ground. Press Esc key to exit...")
}

func (g *TreasureHunt) WatchKey(wg *sync.WaitGroup, game *TreasureHunt) {
	for g.Ctx.Err() == nil {
		c, key, _ := keyboard.GetSingleKey()
		switch key {
		case keyboard.KeyArrowUp:
			game.MoveChar(DNorth)
		case keyboard.KeyArrowRight:
			game.MoveChar(DEast)
		case keyboard.KeyArrowDown:
			game.MoveChar(DSouth)
		case keyboard.KeyArrowLeft:
			game.MoveChar(DWest)
		case keyboard.KeyEsc:
			g.Cancel()
		default:
			if c == 'X' || c == 'x' {
				g.Dig()
			}
		}

		game.Render()
	}

	wg.Done()
}

func main() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	_ = cmd.Run()

	// var mapFilePath string
	helpFlag := getopt.BoolLong("help", 'h', "help")
	fileFlag := getopt.StringLong("file", 'f', "./map.txt", "the map file location")
	getopt.Parse()

	if *helpFlag {
		getopt.Usage()
		os.Exit(0)
	}

	file, err := os.Open(*fileFlag)
	if err != nil {
		panic(err)
	}

	defer file.Close()
	scanner := bufio.NewScanner(file)
	ctx, cancel := context.WithCancel(context.Background())
	game := &TreasureHunt{
		Ctx:    ctx,
		Cancel: cancel,
	}
	var ch *Character
	xMap := make(Map, 0)
	grounds := make([]*Ground, 0)
	ri := 0
	for scanner.Scan() {
		row := []byte(scanner.Text())
		xRow := make([]*MapBlock, len(row))
		for ci, col := range row {
			var comp MapComponent

			switch col {
			case '#':
				comp = new(Obstacle)
			case 'X':
				ch = new(Character)
				ch.HeadTo = DSouth
				ch.X = ci
				ch.Y = ri
				fallthrough // this component is considered as ground too
			case '.':
				fallthrough
			default:
				gr := new(Ground)
				gr.X = ci
				gr.Y = ri
				grounds = append(grounds, gr)
				comp = gr
			}

			xRow[ci] = &MapBlock{Content: comp}
		}

		xMap = append(xMap, xRow)
		ri++
	}

	game.Blocks = xMap
	game.Grounds = grounds
	game.Char = ch

	rd := rand.New(rand.NewSource(time.Now().Unix()))
	for i := 0; i < len(game.Grounds); i = (i + 1) % len(game.Grounds) {
		prob := rd.Intn(100) + 1
		if prob < 10 {
			game.Grounds[i].Treasure = &Treasure{}
			break
		}
	}

	game.Render()

	wg := &sync.WaitGroup{}
	wg.Add(1)

	go game.WatchKey(wg, game)

	wg.Wait()
	if game.TreasureIsFound {
		fmt.Println("Congratulation! You have found the treasure!")
	} else {
		fmt.Println("Bye!")
	}
	fmt.Println()
}
